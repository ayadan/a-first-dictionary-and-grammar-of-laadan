\documentclass[../A First Dictionary and Grammar of Laadan.tex]{subfiles}
 
\begin{document}

A First Dictionary and Grammar of Láadan: Second Edition

    Suzette Haden Elgin

    Edited by Diane Martin

    Society for the Furtherance and Study of Fantasy and Science Fiction, Inc.
    
    Box 1624
    
    Madison, WI 53701-1624
    
    \newpage

    \section*{Other Works by Suzette Haden Elgin}

    \paragraph{Fiction}
    \begin{itemize}
        \item[] The Communipaths
        \item[] Furthest
        \item[] At the Seventh Level
        \item[] Star-Anchored, Star-Angered
        \item[] Twelve Fair Kingdoms
        \item[] The Grand Jubilee
        \item[] And then There'll be Fireworks
        \item[] Yonder Comes the Other End of Time
        \item[] Native Tongue
        \item[] Native Tongue II: The Judas Rose
    \end{itemize}

    \paragraph{Non-Fiction}
    \begin{itemize}
        \item[] Guide to Transformational Grammar (with John Grinder)
        \item[] What is Linguistics?, 1st Edition
        \item[] Pouring Down Words
        \item[] What is Linguistics?, 2nd Edition
        \item[] The Gentle Art of Verbal Self-Defense
        \item[] More on the Gentle Art of Verbal Self-Defense
        \item[] The Last Word on the Gentle Art of Verbal Self-Defense
    \end{itemize}

    The Society for the Furtherance and Study of Fantasy and Science Fiction, Inc (SF$^{3}$)

    Box 1624

    Madison, WI  53701-1624

    \copyright 1988 by Suzette Haden Elgin

    All Rights Reserved

    Second Edition, March 1988

    ISBN 0-9618641-0-9

    \tableofcontents

    \newpage
    \section*{Publisher's Acknowledgements}

    This book was made possible through the efforts of many people whom
    I want to acknowledge and thank: Suzette Haden Elgin, of course,
    for the obvious: the inventing of Láadan, and the writing of this
    book; and also for the not-so-obvious: her support and encouragement
    as I undertook a project that grew to consume most of my spare time
    and energy for far longer than I believed possible.
    Richard S. Russell, my partner of many years, for the use of his
    computer and printer (and for his much-needed advice and assistance).
    Port-to-Print of Madison, WI, for transferring the data files from
    PC to Macintosh format, saving me many hours of re-entry.
    Karen Robinson of Raleigh, NC, for her extensive work in compiling
    the Láadan-to-English Dictionary. Toni Armstrong, of Chicago, IL,
    for permission to reprint lessons originally appearing in
    \textit{Hot Wire} magazine. The many readers of the first edition,
    whose comments and interest kept Láadan alive and growing.
    And the members of SF$^{3}$ for letting me convince them to sponsor
    this project.

    Diane Martin \\
    Madison, Wisconsin

    \paragraph{Techincal Production Notes:} This book was produced
    on a Macintosh II computer using WriteNow, and an AST TurboLaser/PS
    printer. The primary typeface is New Century Schoolbook. The cover
    and interior stock are Nekoosa Textweave. Printed by Odana Press,
    of Madison, WI

    Macintosh is a trademark of Apple Computer, Inc. WriteNow is a trademark licensed to T/Maker Co. TurboLaser is a registered trademark of AST research.

    \chapter{How To Use This Book}

    Láadan is a language constructed by a woman, for women,
    for the specific purpose of expressing the perceptions of women.
    This grammar and dictionary are intended to introduce you to the
    language and give you an opportunity to see if it is of interest to
    you or could be useful to you. Each grammar unit has both a core section
    and a supplementary section that expands on the core. You might find
    it best to read through all the core sections first to get the feel
    and weight of the language, and then return to
    the supplementary sections.

    Vocabulary from the Grammar is listed in the Dictionary section,
    which is divided into an English-to-Láadan section and a (new
    to this edition) Láadan-to-English section; following the Dictionary
    is a set of miscellaneous information, additional vocabulary, and,
    for reference purposes, a brief listing of the basic rules of Láadan grammar.

    \chapter{Introduction: The Construction of Láadan}

    In the fall of 1981, I was involved in several seemingly unrelated
    activites. I had been asked to write a scholarly review of the book
    \textit{Women and Men Speaking}, by Cheris Kramarae; I was working
    on a speech for the WisCon science fiction convention scheduled
    for March 1982, where I was to be Guest of Honor; and I was
    reading - and re-reading - Douglas Hofstadter's \textit{Gödel, Escher, Bach.}
    I had also been reading a series of papers by Cecil Brown and his
    associates on the subject of lexicalization -
    that is, the giving of names (words, in most cases, or parts of words)
    to units of meaning in human languages. Out of this serendipitous mix
    came a number of things.

    (1) I became aware, through Kramarae's book, of the feminist hypothesis that
    existing human languages are inadequate to express the perceptions of women.
    This intrigued me because it had a built-in paradox: if it is true,
    the only mechanism available to women for discussing the problem
    is the very same language(s) alleged to be inadequate for the purpose.

    (2) There occurred to me an interesting possibility within the framework
    of the Sapir-Whorf Hypothesis (briefly, that language structures
    perceptions): if women had a language adequate to express their perceptions,
    it might reflect a quite different reality than that perceived by men.
    This idea was reinforced for me by the papers of Brown \textit{et al.},
    in which there was constant reference to various phenomena of lexicaliation
    as the only natural and self-evident possibilities. I kept thinking that women
    would have done it differently, and that what was being called the
    ``natural'' way to create words seemed to me to be instead the
    \underline{male} way to create words.

    (3) I read in \textit{Gödel, Escher, Bach} a reformulation of
    Gödel's Theorem, in which Hofstadter proposed that for every record player
    there were records it could not play because they would lead to its indirect
    self-destruction. And it struck me that if you squared this you would get a
    hypothesis that for every language there were perceptions it could
    not express becasue they would lead to its indirect self-destruction.
    Furthermore, ifyou cubed it, you would get a hypothesis that for every culture
    there are \underline{languages} it could not use because they would lead to its
    indirect self-destruction. This made me wonder: what would happen to
    American culture if women did have and did use a language that expressed
    their perceptions? Would it self-destruct?

    (4) I focused my Guest of Honor speech for WisCon on the question of
    why women portraying new realities in science fiction had, so far as I knew,
    dealt only with Matriarchy and Androgyny, and never with the third
    alternative based on the hypothesis that women are not superior to
    men (Matriarchy) or interchangeable with and equal to men (Androgyny)
    but rather entirely \underline{different} from men.
    I proposed that it was at least possible that this was because
    the only language available to women \underline{excluded} the third reality.
    Either becasue it was unlexicalized and thus no words existed with
    which to write about it, or it was lexicalized in so cumbersome a manner
    that it was useless for the writing of fiction, or the lack of lexical
    resources literally made it impossible to \underline{imagine} such a reality.

    Somewhere along the way, this all fell together for me, and I found
    myself with a cognitive brew much too fascinating to ignore. The only
    question was how I was to go about exploring all of this. A scientific
    experiment and a scholarly monograph would have been nice;
    but I knew what the prospects of funding would be for an investigation of
    these matters, and I was without the private income that would have let
    me ignore that aspect of the problem. I therefore chose as medium the writing
    of a science fiction novel about a future America in which the woman-language
    had been constructed and was in use. That book, called \textit{Native Tongue}
    was published by DAW Books in August 1984. Its sequel,
    \textit{Native Tongue II: The Judas Rose}, appeared from DAW in February 1987.

    In order to write the book, I felt obligated to at least try to construct
    the language. I'm not an engineer, and when I write about engines I make no
    attempt to pretend that I know how engines are put together or how they
    function. But I \underline{am} a linguist, and knowing how languages work is
    supposed to be my home territory. I didn't feel that I could ethically just
    fake the woman-language, or just insert a handful of hypothetial words
    and phrases to represent it. I needed at least the basic grammar and a modest
    vocabulary, and I needed to experience what such a project would be
    \underline{like}. I therefore began, on June 28, 1982, the construction
    of the language that became Láadan.

    Becuase I am a linguist, I have studied many existing languages,
    from a number of different language families. In the construction of
    Láadan I have tried to use features of those languages which seemed to me to be
    valuable and appropriate. This method of construction is often called
    ``patchwork'', and is not looked upon with great favor in the Patriarchal
    Paradigm that dominates contemporary science. I would remind you,
    nonetheless, that among women the patchwork quilt is recongized as an
    artform, and the methodology of patchwork is respected.

    My original goal was to reach a vocabulary of 1,000 words -
    enough, if well chosen, for ordinary conversation and informal writing.
    I passed that goal early on, and in the fall of 1982 the journal
    \textit{Women and Language News} published the first writing in the
    language, a Nativity story written from Mary's point of view.

    There was one more factor that entered into my decision to construct
    Láadan, and I saved it for last becasue it was not there originally but
    developed out of the work that I was doing. I found myself discussing the
    idea of the woman-language, proposed need for it, etc., at meetings and
    conferences and among my friends and colleagues. And I found that it
    was possible to get the necessary concepts across, if I was patient.
    (There was, for example, the useful fact that English has no word
    whatsoever for what a woman does during the sexual act... this generally
    helps to make some points more clear.) But I got thoroughly tired of
    one question and its answer. People would ask me, ``Well, if existing
    human languages are inadequate to express women's perceptions, why
    haven't they ever made one up that is adequate?''
    And all I could ever say was that I didn't know.
        \footnote{At that time I had not yet had the opportunity to read Mary Daly's
        book, published in May 1984, called \textit{Pure Lust}. In that book Daly
        tells us that St. Hildegarde of Bingen, who lived from 1098-1179, constructed
        a language consisting of 900 words, with an alphabet of 23 letters. She was a
        distinguished scholar, with publications to her credit in a number of fields;
        as Daly says, it is impossible for us to know how much of value was lost to us
        when this language was lost. And I now have an alternative answer to that
        persistent question, although I have no way of knowing whether St. Hildegarde's
        motivation for the construction of her lagnuage was a sense that no language
        adequate to express her perceptions was available to her.}
    This became tiresome, and frustrating, and it was a relief to me when
    I was at last able to way, ``Well, as a matter of fact, a woman
    did construct such a language, beginning on June 28, 1982, and its name is Láadan.''

    This book is a teaching grammar of Láadan, with an accompanying dictionary.
    It is only a beginning, and for all I know, the beginning of a failure,
    something that will never be of interest to anyone but the collector of linguistic exotica.
    But because this book exists, it will be very hard to ``lose''
    Láadan in the way that other languages have been swallowed up by
    the History of Mankind. For that, I am most grateful to the members of SF$^{3}$,
    who thought the workw as important enough to justify publication.

    Suzette Haden Elgin \\
    near Old Alabam, Arkansas

    \chapter{The Sounds of Láadan}

    Láadan was constructed to be simpe to pronounce. This description
    is tailored for speakers of English, because the material is written
    in English; but the sound system ahs been designed to present as few
    difficulties as possible, no matter what the native language of
    the learner. \\

    \begin{tabular}{p{2cm} l l}
        Vowels: & \textbf{a} & as in ``c\underline{a}lm'' \\
                & \textbf{e} & as in ``b\underline{e}ll'' \\
                & \textbf{i} & as in ``b\underline{i}t'' \\
                & \textbf{o} & as in ``h\underline{o}me'' \\
                & \textbf{u} & as in ``d\underline{u}ne''
    \end{tabular} \\

    Consonants: \textbf{b, d, sh, m, n, l, r, w, y, h} - as in English \\

    \begin{tabular}{p{2cm} l l}
                & \textbf{th} & as in ``\underline{th}ink'' \\
                & \textbf{zh} & as in ``plea\underline{s}ure'' \\
    \end{tabular} \\

    There is one more consonant in Láadan; it is \textit{``lh''}
    and it has no English equivalent. If you put the tip of your
    tongue firmly against the roof of your mouth at the point where
    it begins to arch upward, draw the corners of your lips back as you
    would for an exaggerated smile, and try to say English ``sh'',
    the result should be an adequate \textit{``lh''}. It is a sound
    with a hissing quality, and is not especially pleasant to hear.
    In Láadan it occurs only in words that are themselves references
    to something unpleasant, and can be added to words to give them
    a negative meaning. This is patterned after a similar feature of
    Navajo, and is something so very handy that I have always wished
    it existed in English.

    When a Láadan vowel is written with an accent mark above it,
    it is a vowel with high tone. English doesn't have any tones,
    but that will be no problem for you, since you can express it as
    heavy stress. Think of the way that you distinguish the noun
    ``convert'' from the very ``convert'' by stressing one of the
    two syllables. If you pronouns a high-toned Láadan vowel as you
    would pronounce a strongly-stressed English syllable, you will
    achieve the same effect as high tone. Because Láadan does not use
    English stress, this will not be a source of confusion.

\end{document}

